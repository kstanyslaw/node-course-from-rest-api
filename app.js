const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const multer = require('multer');

const MONGODB_URI = 'mongodb+srv://dbUser:dbUserPassword@demo-ltmsk.azure.mongodb.net/messages';

const app = express();

const feedRoutes = require('./routes/feed');
const authRoutes = require('./routes/auth');

// MULTER STORAGE CONFIG
const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'images');
    },
    filename: (req, file, cb) => {
        cb(null, new Date().toISOString() + '-' + file.originalname);
    }
});

// MULTER FILE FILTER
const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

app.use(bodyParser.json());
app.use('/images', express.static(path.join(__dirname, 'images')));
app.use(multer({ storage: fileStorage, fileFilter: fileFilter }).single('image'));

// CORS RULES
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

// ROUTES HANDLER
app.use('/feed', feedRoutes);
app.use('/auth', authRoutes);

// ERROR HANDLER
app.use((error, req, res, next) => {
    console.log(error);
    const data = error.data;
    const status = error.statusCode || 500;
    const message = error.message;
    res.status(status).json({ message: message, data: data });
})

// MONGODB CONNECT & START LISTEN PORT
mongoose.connect(MONGODB_URI + '?retryWrites=true&w=majority', { useNewUrlParser: true })
    .then((result) => {
        app.listen(8080);
    })
    .catch((err) => {
        console.log(err);
    })

